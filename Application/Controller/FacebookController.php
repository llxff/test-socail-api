<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 06.11.12
 * Time: 23:41
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Controller;
use Controller\Abstraction\Controller;
use Application\View\ViewModel\AsJsonResult;

class FacebookController extends Controller
{

    public function Index()
    {
        return $this->View('Enter fql query');
    }

    public function ShowQuery($fql)
    {
        require ROOTDIR . '/Vendor/sdk/facebook/src/facebook.php';

        $this->View->Data['query'] = $fql;

        $facebook = new \Facebook(array(
            'appId'  => '344617158898614',
            'secret' => '6dc8ac871858b34798bc2488200e503d',
        ));

        $result = $facebook->api(array(
            'method' => 'fql.query',
            'query' => $fql
        ));

        return $this->ViewOf('Index', new AsJsonResult($result));
    }

    public function onException(\Exception $e)
    {
        return $this->ViewOf('Index', 'Error: wrong query'.PHP_EOL.$e->getMessage());
    }
}
