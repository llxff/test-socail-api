<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 07.11.12
 * Time: 0:42
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Controller;
use Controller\Abstraction\Controller;
use Vendor\Jill\SessionStorage;
use Application\View\ViewModel\JsonResult;

class FoursquareController extends Controller
{
    private $Foursquare;
    private $acess_token;

    function __construct()
    {
        require  ROOTDIR . '/Vendor/sdk/foursquare/src/FoursquareAPI.class.php';

        $this->Foursquare = new \FoursquareAPI('CDVZ2TBVIPK5MDF4RYJDF03DEGTWDUWPVYDIY30UZRDD00D2','WCTQGZOUTN0Y4YDONVIICJ4LKPBGEHDDGYZGQ32BUJAJXH0C');

        $this->acess_token = SessionStorage::storage()->getTemp('token');
    }

    public function beforeRun()
    {
        $this->View->Data['token'] = $this->acess_token;
        $this->View->Data['link'] = $this->Foursquare->AuthenticationLink('http://'.$_SERVER['SERVER_NAME'].'/apitest/Foursquare/Auth/');
    }

    public function Index()
    {
        return $this->View('Enter data');
    }

    public function ShowQuery($method, $params)
    {
        $params = json_decode($params, true);

        if($this->acess_token)
        {
            if(!is_array($params))
            {
                $params = array();
            }

            $params = $params + array('oauth_token' => $this->acess_token);
        }

        $result = $this->Foursquare->GetPublic($method, $params);

        $this->View->Data["method"] = $method;
        $this->View->Data["params"] = json_encode($params);

        return $this->ViewOf('Index', new JsonResult($result) );
    }

    public function onException(\Exception $e)
    {
        return $this->ViewOf('Index', '4square api sucks'.PHP_EOL.$e->getMessage());
    }

    public function Auth($code)
    {
        $token = $this->Foursquare->GetToken($code, 'http://'.$_SERVER['SERVER_NAME'].'/apitest/Foursquare/Auth/');

        if($token)
        {
            SessionStorage::storage()->setTemp('token', $token, 60 * 60 * 24);
        }

        return $this->Redirect('/apitest/Foursquare/');
    }


}
