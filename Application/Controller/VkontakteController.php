<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 12.11.12
 * Time: 11:24
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Controller;
use Controller\Abstraction\Controller;
use Vendor\Jill\SessionStorage;
use Application\View\ViewModel\JsonResult;

class VkontakteController extends Controller
{
    private $Storage;
    const VK_DATA = 'vk_api_data';

    function __construct()
    {
        $this->Storage = SessionStorage::storage();
    }

    public function Index()
    {
        if($this->Storage->getTemp(self::VK_DATA))
        {
            return $this->View();
        }
        else
        {
            return $this->Redirect('https://oauth.vk.com/authorize?client_id=3233602&scope=&redirect_uri=http://'.$_SERVER['SERVER_NAME'].'/apitest/Vkontakte/Convert/&display=page&response_type=token');
        }
    }

    public function Convert($error, $error_description)
    {
        if($error === null)
        {
            return $this->View();
        }

        return $error.'<br />'.$error_description;
    }

    public function Register()
    {
        $this->Storage->setTemp(self::VK_DATA, $_GET, $_GET['expires_in']);
        return $this->Redirect('/apitest/Vkontakte/');
    }

    public function ShowQuery($method, $params)
    {
        if($vkData = $this->Storage->getTemp(self::VK_DATA))
        {
            $this->View->Data = $_GET;

            $apiString = "https://api.vk.com/method/$method?";

            $params = json_decode($params, true) ?: array();

            $apiString .= http_build_query( $params + array('access_token' => $vkData['access_token']));

            return $this->ViewOf('Index', new JsonResult(file_get_contents($apiString)));
        }

        return $this->Redirect('/apitest/Vkontakte/');
    }

    public function onException(\Exception $e)
    {
        return $this->ViewOf('Index', 'error in query'.PHP_EOL.$e->getMessage());
    }
}
