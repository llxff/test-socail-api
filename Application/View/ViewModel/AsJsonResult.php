<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 12.11.12
 * Time: 21:42
 * To change this template use File | Settings | File Templates.
 */
namespace Application\View\ViewModel;

class AsJsonResult extends JsonResult
{
    function __construct($data, $prettyPrint = true)
    {
        parent::__construct(json_encode($data), $prettyPrint);
    }
}
