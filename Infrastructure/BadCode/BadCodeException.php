<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 07.11.12
 * Time: 1:05
 * To change this template use File | Settings | File Templates.
 */
namespace Infrastructure\BadCode;

class BadCodeException extends \Exception
{
    public function __construct($message, $errorLevel = 0, $errorFile = '', $errorLine = 0)
    {
        parent::__construct($message, $errorLevel);

        $this->file = $errorFile;
        $this->line = $errorLine;
    }
}
